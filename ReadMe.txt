Utility helper

1. Please execute "sudo ./ module.sh" to compile and install the module. Make sure you've got admin rights.
2. Verify with "lsmod | grep timer" whether the timer module is loaded
into the kernel.
3. You could start "timerf" by typing "echo "s" > /dev/timerf". Verify the 
procedure with "cat /dev/timerf". In case of success, you should see the 
counter going up. Instead of "s" for start, one could enter "p" for pause,
"c" for continue, "r" to reset the counter.
4. You could start "timerr" by typing "echo "l length "> /dev/timerr". Length
could be any valide number in the range of long determined by your system.
 Use "s" to start the decreasing counter. By typing "echo "s" > /dev/timerr" the decreasing counter will be displayed. To stop or reset the routine act analog
to the steps of 3. for timerf.
5. To get rid of the module "sudo make clean" is enough.
Have fun;)
