DEBFLAGS = -O2

EXTRA_CFLAGS = $(DEBFLAGS)
EXTRA_CFLAGS += -I$(LDDINC)
#TIMER_DEBUG=""
ifdef DEBUG
ifeq ($(DEBUG),1)
TIMER_DEBUG = -DTIMER_DEBUG=1
else 
TIMER_DEBUG = -DTIMER_DEBUG=0
endif
else
TIMER_DEBUG = -DTIMER_DEBUG=0
endif

ifneq ($(KERNELRELEASE),)

timer-objs := main.o

obj-m	:= timer.o

else

KERNELDIR ?= /lib/modules/$(shell uname -r)/build
PWD       := $(shell pwd)

add_module:
	$(MAKE) -C  $(KERNELDIR) M=$(PWD) LDDINC=$(PWD)/../include modules CC="$(CC) $(TIMER_DEBUG)"
endif

load:
	/sbin/insmod timer.ko



clean:
	rm -rf *.o *~ core .depend .*.cmd *.ko *.mod.c .tmp_versions modules.order Module.symvers

depend .depend dep:
	$(CC) $(CFLAGS) -DTIMER_DEBUG=1 -M *.c > .depend
	echo "$(CC) $(CFLAGS) -DTIMER_DEBUG=1 -M *.c > .depend"


ifeq (.depend,$(wildcard .depend))
include .depend
endif
