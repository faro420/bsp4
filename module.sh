if [ "$1" = "DEBUG=1" ]; then
make DEBUG=1
else
make DEBUG=0
fi
rmmod timer
rm /dev/timerf
rm /dev/timerr
insmod timer.ko
timerNr=$(cat /proc/devices | grep timer | cut -d" " -f1 | tail -1)
mknod -m 777 /dev/timerf c $timerNr 0
mknod -m 777 /dev/timerr c $timerNr 1


