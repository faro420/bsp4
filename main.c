#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <asm/uaccess.h>        
#include <linux/jiffies.h>
#include <linux/string.h>       
#include <linux/fcntl.h>
#include <linux/seq_file.h>
#include <linux/cdev.h>
#include <linux/semaphore.h>
#include "timer.h"

MODULE_LICENSE("GPL");
#define SUCCESS 0
#define DEVICE_NAME "timer"     /* Dev name as it appears in /proc/devices   */
#define BUF_LEN 80              /* Max length of the message from the device */

/* 
 * Global variables are declared as static, so are global within the file. 
 */

static int Major; /* Major number assigned to our device driver */
static char msg[BUF_LEN]; /* The msg the device will give when asked */
static char *msg_Ptr;
static struct timerStruct timerdev[NUM_OF_DEVS];
static int minor;
static struct file_operations fops = { .read = device_read, .write =
		device_write, .open = device_open, .release = device_release };

struct device {
	char array[BUF_LEN];
} char_dev;

/*
 * This function is called when the module is loaded
 */
int init_module(void) {
	Major = register_chrdev(0, DEVICE_NAME, &fops);
	// jifOld = get_jiffies_64();
	if (Major < 0) {
		PDEBUG("char device registration failed %d\n", Major);
		return Major;
	}
	fillStruct();

	PDEBUG("I was assigned major number %d. To talk to\n", Major);
        PDEBUG("the driver, create a dev file with\n");
        PDEBUG("'mknod /dev/%s c %d 0'.\n", DEVICE_NAME, Major);
        PDEBUG("Try v/coarious minor numbers. Try to cat and echo to\n");
        PDEBUG("the device file.\n");
        PDEBUG("Remove the device file and module when done.\n");
	return SUCCESS;
}

/*
 * This function is called when the module is unloaded
 */
void cleanup_module(void) {
	/* 
	 * Unregister the device 
	 */
	unregister_chrdev(Major, DEVICE_NAME);

}

/*
 * Methods
 */

/* 
 * Called when a process tries to open the device file, like
 * "cat /dev/mycharfile"
 */
static int device_open(struct inode *inode, struct file *filp) {
	minor = iminor(filp->f_path.dentry->d_inode);

	PDEBUG("Device_open on minorNr: %d\n", minor);

	if (down_interruptible(&timerdev[minor].sem)) {
		return -ERESTARTSYS;
	}
	try_module_get(THIS_MODULE);
	up(&timerdev[minor].sem);

	return 0;
}
/* 
 * Called when a process closes the device file.
 */
static int device_release(struct inode *inode, struct file *filp) {
	minor = iminor(filp->f_path.dentry->d_inode);
	/* 
	 * Decrement the usage count, or else once you opened the file, you'll
	 * never get get rid of the module. 
	 */
	//minor = iminor(filp->f_path.dentry->d_inode);
	PDEBUG("Device_Release on minorNr: %d\n",minor);
	module_put(THIS_MODULE);
	return 0;
}

/* 
 * Called when a process, which already opened the dev file, attempts to
 * read from it.
 */
static ssize_t device_read(struct file *filp, char *buffer, size_t length,
		loff_t * offset) {
	/*
	 * Number of bytes actually written to the buffer 
	 */
	int bytes_read = 0;
	minor = iminor(filp->f_path.dentry->d_inode);
	PDEBUG("Device_read from minorNr: %d\n", minor);

	if (down_interruptible(&timerdev[minor].sem)) {
		return -ERESTARTSYS;
	}

	/*
	 *
	 * If we're at the end of the message, 
	 * return 0 signifying end of file 
	 */

	update_counter();
	sprintf(msg, "Counter := %lu\n", timerdev[minor].counter);
	msg_Ptr = msg;

	if (*msg_Ptr == 0)
		return 0;

	/* 
	 * Actually put the data into the buffer 
	 */
	while (length && *msg_Ptr) {

		/* 
		 * The buffer is in the user data segment, not the kernel 
		 * segment so "*" assignment won't work.  We have to use 
		 * put_user which copies data from the kernel data segment to
		 * the user data segment. 
		 */
		put_user(*(msg_Ptr++), buffer++);

		length--;
		bytes_read++;
	}

	/* 
	 * Most read functions return the number of bytes put into the buffer
	 */

	up(&timerdev[minor].sem);

	return bytes_read;
}

/*  
 * Called when a process writes to dev file: echo "hi" > /dev/hello 
 */
static ssize_t device_write(struct file *filp, const char *buff, size_t len,
		loff_t * off) {

	unsigned long return_flag;
	int i = 0;

	minor = iminor(filp->f_path.dentry->d_inode);

	if (down_interruptible(&timerdev[minor].sem)) {
		return -ERESTARTSYS;
	}

	return_flag = copy_from_user(char_dev.array, buff, len);

	if (return_flag != 0) {
		PDEBUG( "Error copy_from_user");
		return -EFAULT;
	}

	PDEBUG( "Device_write from minorNr: %d\n", minor);

	if (minor == TIMERF) {
		if (strchr(char_dev.array, 's')) {
			if (timerdev[minor].counter == 0) {
				PDEBUG("Start timerf - > s\n");
				start_counter();
				timerdev[minor].started = 1;
			}
		}
	} else if (minor == TIMERR) {
		if (strchr(char_dev.array, 'l')) {
			if ((timerdev[minor].breakTimer == 1)
					&& (timerdev[minor].started == 0)) {
				char *cmdchar = strchr(char_dev.array, 'l');
				int success = 0;
				unsigned long timeval = 0;
				PDEBUG( "load Timer - > l\n");
				success = sscanf(cmdchar + 1, "%lu", &timeval);
				if (success == 1) {
					timerdev[minor].counter = timeval;
					timerdev[minor].loaded = 1;
				}
			}

		}
		if (strchr(char_dev.array, 's')) {
			if ((timerdev[minor].counter != 0) && (timerdev[minor].loaded == 1)
					&& (timerdev[minor].started == 0)) {
				start_counter();
				timerdev[minor].started = 1;
				PDEBUG("start timerr - > s\n");
			}
		}

	}

	if (strchr(char_dev.array, 'p')) {
		update_counter();
		PDEBUG("pause - > p\n");
		timerdev[minor].breakTimer = 1;
	}
	if (strchr(char_dev.array, 'c')) {
		if ((timerdev[minor].counter > 0) && (timerdev[minor].started == 1)) {
			timerdev[minor].breakTimer = 0;
			timerdev[minor].jifNew = get_jiffies_64();
			timerdev[minor].jifOld = timerdev[minor].jifNew;
			PDEBUG("continue - > c\n");
		}
	}
	if (strchr(char_dev.array, 'r')) {
		PDEBUG("reset - > r\n");
		timerdev[minor].counter = 0;
		timerdev[minor].jifOld = get_jiffies_64();
		timerdev[minor].breakTimer = 1;
		timerdev[minor].started = 0;
	}

	//clean char array
	while (i < len) {

		char_dev.array[i] = 0;
		i++;
	}

	up(&timerdev[minor].sem);

	return len;
}

static void update_counter(void) {
	PDEBUG("update counter on minor %d\n",minor);

	timerdev[minor].jifNew = get_jiffies_64();
	if (timerdev[minor].breakTimer == 0) {
		if (minor == TIMERF) {
			timerdev[minor].counter = timerdev[minor].counter
					+ (timerdev[minor].jifNew - timerdev[minor].jifOld);
		} else if (minor == TIMERR) {
			long diff = timerdev[minor].jifNew - timerdev[minor].jifOld;
			if (diff > timerdev[minor].counter) {
				timerdev[minor].counter = 0;
			} else {
				timerdev[minor].counter -= diff;
			}

		}
	}
	timerdev[minor].jifOld = timerdev[minor].jifNew;

}

static void fillStruct(void) {
	int i = 0;
	timerdev[0].minor = 0;
	timerdev[1].minor = 1;
	while (i < NUM_OF_DEVS) {
		timerdev[i].counter = 0;
		timerdev[i].jifOld = 0;
		timerdev[i].jifNew = 0;
		timerdev[i].breakTimer = 1;
		timerdev[i].loaded = 0;
		timerdev[i].started = 0;
		sema_init(&timerdev[i].sem, 1);
		i++;
	}
}

static void start_counter(void) {
	timerdev[minor].jifNew = get_jiffies_64();
	timerdev[minor].jifOld = timerdev[minor].jifNew;
	timerdev[minor].breakTimer = 0;

}
