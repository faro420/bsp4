
//#define TIMER_DEBUG
#undef PDEBUG             /* undef it, just in case */
#if TIMER_DEBUG == 1
#  ifdef __KERNEL__
/* This one if debugging is on, and kernel space */
#    define PDEBUG(fmt, args...) printk( KERN_DEBUG "timer: " fmt, ## args)
#  else
/* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef PDEBUGG

#define NUM_OF_DEVS 2
#define TIMERF 0
#define TIMERR 1

int init_module(void);

void cleanup_module(void);

static int device_open(struct inode *, struct file *);

static int device_release(struct inode *, struct file *);

static ssize_t device_read(struct file *, char *, size_t, loff_t *);

static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

static void fillStruct(void);

static void update_counter(void);

static void start_counter(void);

struct timerStruct {
	int minor;
	unsigned long counter;
	unsigned long jifOld;
	unsigned long jifNew;
	int breakTimer;
	int loaded;
	int started;
	struct semaphore sem;
};

